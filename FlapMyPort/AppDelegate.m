//
//  AppDelegate.m
//  FlapMyPort
//
//  Created by Владислав Павкин on 08/08/16.
//  Copyright © 2016 Vladislav Pavkin. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

- (IBAction)FlapsMenuClicked:(id)sender {
    
    NSApplication *App = [NSApplication sharedApplication];

    for (NSWindow *Window in App.windows)
    {
        if([Window.identifier isEqualToString:@"MainWindow"])
        {
            [Window makeKeyAndOrderFront:self];
        }
    }

 }

- (IBAction)BackgroundMenuClicked:(id)sender {
    
    NSApplication *App = [NSApplication sharedApplication];
    
    for (NSWindow *Window in App.windows)
    {
        if([Window.identifier isEqualToString:@"MainWindow"])
        {
            [Window close];
        }
    }
}

- (BOOL) applicationShouldHandleReopen:(NSApplication *)sender hasVisibleWindows:(BOOL)flag
{
    /*
    if ( visibleWindows )
    {
        [self.window orderFront:self];
    }
    else {
        [self.window makeKeyAndOrderFront:self];
    }
    */

    for (NSWindow *Window in sender.windows)
    {
        if(!flag)
        {
            if([Window.identifier isEqualToString:@"MainWindow"])
            {
                [Window makeKeyAndOrderFront:self];
            }
        }
    }
    
    return YES;
}

@end
